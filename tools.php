<?php
include('./includes/init.php');
require "./includes/loginheader.php";

$page['id'] = "tools";
$page['title'] = "Tools";

include('views/page-header-view.php');
include('views/tools-view.php');
include('views/page-footer-view.php');
