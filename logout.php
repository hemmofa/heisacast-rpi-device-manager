<?php
	setcookie("password", '', strtotime('+2 seconds'));
    session_start();
	$_SESSION["loggedin"] = false;
    session_destroy();

header('Location: index.php');
exit;