<div class="account-header">
	<div class="account-header-container">
		<div class="account-username"><h3>System Status</h3></div>		
	</div>
</div>      
<div id="contentnotloaded">
	<div class="drop-empty">Gathering data...</div>
</div>
<div id="contentloaded">  
	<div class="drop">
		<h3>Network</h3>
		<div class="statusitem">
			<p class="statustitle">Hostname</p>
			<p class="statusvalue" id="network-hostname"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">IP-Address</p>
			<p class="statusvalue" id="network-ipaddresses"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">Wi-Fi Accespoint Name</p>
			<p class="statusvalue" id="wifi-accesspoint"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">Wi-Fi Link Quality</p>
			<p class="statusvalue" id="wifi-connection-linkquality"></p>
		</div>
	</div>
	<div class="drop">
		<h3>Memory & Storage</h3>
		<div class="statusitem">
			<p class="statustitle">Uptime</p>
			<p class="statusvalue" id="os-uptime"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">Memory</p>
			<p class="statusvalue" id="os-memory-total"> (<span id="os-memory-used"></span> used / <span id="os-memory-free"></span> free)</p>
		</div>
		<div class="statusitem">
			<p class="statustitle">Storage</p>
			<p class="statusvalue" id="os-storage">(<span id="os-storage-used"></span> used / <span id="os-storage-free"></span> free)</p>
		</div>
	</div>        
	<div class="drop">
		<h3>System Information</h3>
		<div class="statusitem">
			<p class="statustitle">Raspberry Pi Model</p>
			<p class="statusvalue" id="rpi-model"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">Raspberry Pi Serial Number</p>
			<p class="statusvalue" id="rpi-serialnumber"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">CPU Model</p>
			<p class="statusvalue" id="rpi-cpu-name"></p>
		</div>
		<div class="statusitem">
			<p class="statustitle">CPU Temperature</p>
			<p class="statusvalue" id="rpi-cpu-temp"></p>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
	
		(function($) {
			$.fn.invisible = function() {
				return this.each(function() {
					$(this).css("display", "none");
				});
			};
			$.fn.visible = function() {
				return this.each(function() {
					$(this).css("display", "block");
				});
			};
	}(jQuery));
	});
		function loadSystemStatus() {	
			
			//var getStatusResult = $.ajax({url: "http://131.155.248.156/do/status.php", success: function(result){
			var getStatusResult = $.ajax({url: "/do/status.php", success: function(result){
			systemstatus = JSON.parse(result);
			var numsystemstatus = systemstatus.length;

			$.each(systemstatus, function( key, value ) {
				if($.isArray(value)) {
					//$("#" + key).html('Array not yet supported');
					//.each(value, function( index, value ) {
					 $("#" + key).html(value[0]);
					//}
					
				} else {
					 $("#" + key).html(value);
				}
			});
			$("#contentnotloaded").invisible();
			$("#contentloaded").visible();
			}}); 
		}
setTimeout(loadSystemStatus,1);
</script>
