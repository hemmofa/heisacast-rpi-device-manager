<div class="account-header">
	<div class="account-header-container">
		<div class="account-username"><h3>Tools</h3></div>		
	</div>
</div>        

<div class="drop">
	<h3>Display settings</h3>
	<div class="statusitem">
		<p class="statustitle">Display Status</p>
		<p class="statusvalue">Builtin Touchscreen Display Connected</p>
	</div>

	<div class="statusitem">
		<p class="statusvalue">Display Brightness</p>
		<input type="range" min="11" max="255" value="50" class="rangeslider" id="sliderBrightness">
	</div>
</div>
<div class="drop"><h3>System actions</h3>
	<div class="clickableitem"  onclick="javascript:sendReboot('desktop');">
		<p class="clickabletitle">Exit Kiosk Mode</p>
		<p class="clickabledescription">Exit Chromium and go to the Linux Desktop</p>
	</div>
	<div class="clickableitem"  onclick="javascript:sendReboot('reboot');">
		<p class="clickabletitle">Reboot</p>
		<p class="clickabledescription">Shutdown and reboot the system</p>
	</div>

</div>

<script>
function sendReboot(cmd) {
	$("#shutdown-overlay").fadeIn();
	if(cmd=='reboot') {
		$.getJSON('/do/reboot.php?reboot=yes', function (data) {});	
	} else if (cmd == 'desktop') {
		$.getJSON('/do/reboot.php?desktop=yes', function (data) {});	
	}
}
		
function setBrightness(brightness) {
	$.getJSON('/do/brightness.php?brightness=' + brightness, function (data) {
		if(data.status == "success") {
			console.log("Set Brightness " + brightness + " succes");
		} else {
			toastError("Brightness could not be changed");
			console.log("Set Brightness " + brightness + " failed");
		}
	});
}

var slider = document.getElementById("sliderBrightness");
slider.oninput = function() {
	setBrightness(this.value);
}

		
</script>