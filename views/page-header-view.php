<!DOCTYPE html>
<html>
<head>

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $config['publicpath']; ?>img/favicon/apple-touch-icon.png?v=vMOKm090vx">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $config['publicpath']; ?>img/favicon/favicon-32x32.png?v=vMOKm090vx">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $config['publicpath']; ?>img/favicon/favicon-16x16.png?v=vMOKm090vx">
<link rel="manifest" href="<?php echo $config['publicpath']; ?>img/favicon/site.webmanifest?v=vMOKm090vx">
<link rel="mask-icon" href="<?php echo $config['publicpath']; ?>img/favicon/safari-pinned-tab.svg?v=vMOKm090vx" color="#4ca0ff">
<link rel="shortcut icon" href="<?php echo $config['publicpath']; ?>img/favicon/favicon.ico?v=vMOKm090vx">
<meta name="apple-mobile-web-app-title" content="Device Manager">
<meta name="application-name" content="Device Manager">
<meta name="msapplication-TileColor" content="#4ca0ff">
<meta name="msapplication-config" content="<?php echo $config['publicpath']; ?>img/favicon/browserconfig.xml?v=vMOKm090vx">
<meta name="theme-color" content="#4ca0ff">
<meta content="width = device-width" name="viewport"/>
<meta content="initial-scale=1.0, user-scalable=1" name="viewport"/>
<meta http-equiv="Content-Language" content="nl">
<title><?php echo $config['product']; ?></title>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" media="screen" type="text/css" href="./css/style.css?0.4"/>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
</head>
<body>

<div id="heading">
	<div class="container">
		<div id="headingTitle">
			<h2 style="display:inline;margin-left:4px;">
				<a href="./"><?php echo $config['product']; ?></a></h2>
		</div>
	</div>
</div>

<div id="hbar">
	<div class="container">
		<ul id="menu">
			<li<?php if($page['id']=="status") { ?> class="active-generic"<?php } ?>><a href="./" accesskey="1">Status</a></li>
			<?php if($_SESSION["loggedin"]) { ?>
			<li<?php if($page['id']=="tools") { ?> class="active-generic"<?php } ?>><a href="./tools.php" accesskey="2">Tools</a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear"></div>
</div>



<div id="page">
	<div class="container">
