<?php
    // Simple password protection
    if (!isset($_COOKIE['password']) || password_verify($_COOKIE['password'], $config['password'])) {
	if(!isset($needlogin)) {
        header('Location: login.php');
        exit;
	}
    } else {
		session_start();
		$_SESSION["loggedin"] = true;
	}
?>