<?php
include('./includes/init.php');
if(isset($_GET['generatepassword'])) {
		$options = ['cost' => 11,'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)];
		echo password_hash($_GET['generatepassword'], PASSWORD_BCRYPT, $options);
		die();
	}
$needlogin = 0;

if (isset($_POST['password']) && password_verify($_POST['password'], $config['password'])) {
	setcookie("password", $config['password'], strtotime('+30 days'));
	session_start();
	$_SESSION["loggedin"] = true;
	header('Location: index.php');
	exit;
}



$page['id'] = "login";
$page['title'] = "Login";

include('views/page-header-view.php');
include('views/login-form-view.php');
include('views/page-footer-view.php');
